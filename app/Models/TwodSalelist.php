<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TwodSalelist extends Model
{
    use HasFactory;

    public function twod() {
        return $this->belongsTo( Twod::class);
    }
}
