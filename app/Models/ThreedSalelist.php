<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThreedSalelist extends Model
{
    use HasFactory;

    public function threed() {
        return $this->belongsTo(Threed::class);
    }
}
