<?php

namespace App\Models;

use App\Models\LonePyaing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LonepyaingSalelist extends Model
{
    use HasFactory;

    public function lonepyaing() {
        return $this->belongsTo(LonePyaing::class);
    }
}
