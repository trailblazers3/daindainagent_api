<?php

namespace App\Http\Controllers\Api;

use App\Models\TwodSalelist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AcceptedTransitionController extends Controller
{
    public function twodAcceptedTransitions()
    {
        $user = auth()->user();

        $sale_lists = DB::select("select ts.*,t.number as number from twod_salelists ts, twods t where ts.twod_id = t.id and ts.client_id=$user->id and  ts.status <> 0");


        // $twod_sale_lists = TwodSalelist::with('twod')->whereHas('twod_salelists', function ($query) use ($user) {
        //     $query->where('status', '=', 1);
        //     $query->where('client_id', '=', $user->id);
        // })->get();

        if ($user) {
            return response()->json([
                'status' => 200,
                'twod_sale_lists' => $sale_lists
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized'
            ]);
        }
    }

    public function threedAcceptedTransitions()
    {
        $user = auth()->user();

        $sale_lists = DB::select("select ts.*,t.number as number from threed_salelists ts, threeds t
                                  where ts.threed_id = t.id
                                  and ts.client_id='$user->id'
                                  and  ts.status <> 0");

        if ($user) {
            return response()->json([
                'status' => 200,
                'threed_sale_lists' => $sale_lists
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized'
            ]);
        }
    }

    public function lonepyaingAcceptedTransitions()
    {
        $user = auth()->user();

        $sale_lists = DB::select("select ts.*,t.number as number from lonepyaing_salelists ts, lone_pyaings t
                                  where ts.lonepyaing_id = t.id
                                  and ts.client_id='$user->id'
                                  and  ts.status <> 0");

        if ($user) {
            return response()->json([
                'status' => 200,
                'lonepyaing_sale_lists' => $sale_lists
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized'
            ]);
        }
    }
}
