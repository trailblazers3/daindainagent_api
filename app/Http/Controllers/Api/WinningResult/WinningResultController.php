<?php

namespace App\Http\Controllers\Api\WinningResult;

use App\Http\Controllers\Controller;
use App\Models\TwodSalelist;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WinningResultController extends Controller
{
    public function checkResult()
    {

        $twod_salelists = TwodSalelist::where('status', 1)->with('twod')->get();


        // return $twod_salelists;

        // $twod_salelists->twod->number;
        // foreach ($twod_salelists as $twod_salelist) {
        //     echo $twod_salelist->twod->number . " ";
        // }

        $file_path = public_path() . '/winning_result/result.json';
        $raw_data = file_get_contents($file_path);
        $json_data =  json_decode($raw_data);

        $result_time = Carbon::now('Asia/Yangon')->toTimeString();

        foreach ($json_data->result as $result) {
            if ($result->open_time == '12:01:00') {
                // return "----------HI ---------------";
                //return $result->twod;
                foreach ($twod_salelists as $twod_salelist) {
                   if($twod_salelist->twod->number == $result->twod) {
                        $twod_salelist = TwodSalelist::findOrFail($twod_salelist->id);
                        $twod_salelist->winning_status = 1;
                        $twod_salelist->update();

                        // return response()->json()
                        echo $twod_salelist->id . "<br>";
                        echo "Your number is  " . $twod_salelist->twod->number . ' and Winning resut is ' . $result->twod . "/";
                   }else {
                    echo "Ma Nyi bar  ";
                   }
                }
            }
        }
    }
}
