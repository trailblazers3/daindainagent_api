<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Twod;
use App\Models\User;
use Tymon\JWTAuth\JWT;
use App\Models\TableRequest;
use App\Models\TwodSalelist;
use Illuminate\Http\Request;
use App\Models\ThreedSalelist;
use App\Models\LonepyaingSalelist;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    //checkPhone
    public function checkPhone(Request $request)
    {
        $user = User::where('phone', $request->phone)->get();
        if (!$user) {
            return response()->json([
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return response()->json([
            'status' => 403,
            'message' => 'This phone number is already registered'
        ]);
    }

    // User Register
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|min:6|max:9',
            'password_confirmation' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation Error!',
                'data' => $validator->errors()
            ]);
        }

        $user = new User();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status' => 200,
            'message' => 'Registration success, Plesae Login',
            'data' => $user
        ]);
    }

    // User Login
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required|min:6|max:9'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'data' => $validator->errors()
            ]);
        }

        $token = auth()->attempt($validator->validated());
        if (!$token) {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized'
            ]);
        }

        return $this->createNewToken($token);
    }

    // See Agent Profile
    public function agentProfile()
    {
        $user = auth()->user();
        // $twod_sale_lists = TwodSalelist::with('twod')->whereHas('twod', function ($query) use ($user) {
        //     $query->where('status', '<>', 0);
        //     $query->where('client_id', '=', $user->id);
        // })->get();

        // $threed_sale_lists = ThreedSalelist::with('threed')->whereHas('threed', function ($query) use ($user) {
        //     $query->where('status', '<>', 0);
        //     $query->where('client_id', '=', $user->id);
        // })->get();

        // $lonepyaing_sale_lists = LonepyaingSalelist::with('lonepyaing')->whereHas('lonepyaing', function ($query) use ($user) {
        //     $query->where('status', '<>', 0);
        //     $query->where('client_id', '=', $user->id);
        // })->get();

        $twod_sale_lists = TwodSalelist::where('status', '<>' , 0)->with('twod')->get();
        $threed_sale_lists = ThreedSalelist::where('status', '<>', 0)->with('threed')->get();
        $lonepyaing_sale_lists = LonepyaingSalelist::where('status', '<>', 0)->with('lonepyaing')->get();


        if ($user) {
            return response()->json([
                'status' => 200,
                'user' => $user,
                'twod_saleLists' =>  $twod_sale_lists,
                'threed_salelists' => $threed_sale_lists,
                'lonepyaing_salelists' => $lonepyaing_sale_lists
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized user'
            ]);
        }
    }

    // Profile Update
    public function profileUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validatioin Error',
                'data' => $validator->errors()
            ]);
        }

        $user = auth()->user();
        if ($user) {
            if ($request->hasFile('profile_image')) {
                $file = $request->file('profile_image');
                $imageName = uniqid() . '_' .  $file->getClientOriginalName();
                Storage::disk('public')->put('profiles/' . $imageName, file_get_contents($file));
            }
            $user->name = $request->name;
            $user->image =  $imageName ?? $user->image;
            // $user->password = $request->password ? Hash::make($request->password) : $user->password;
            $user->update();
            return response()->json([
                'status' => 200,
                'message' => 'Your profile is updated successfully!',
                'data' => $user
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized'
            ]);
        }
    }

    // Request For promotion
    public function promoteRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'request_type' => 'required',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => "Validation Error",
                'data' => $validator->errors()
            ]);
        }

        $tableRequest = new TableRequest();

        $user = auth()->user();

        if ($request->phone == $user->phone) { // Check Phone from database
            $tableRequest->client_id = auth()->user()->id; // Need to fix
            $tableRequest->name = auth()->user()->name;
            $tableRequest->phone = auth()->user()->phone;
            $tableRequest->referee_id = $request->referee_id ?? NULL;
            $tableRequest->operationstaff_id = $request->operationstaff_id ?? NULL;
            $tableRequest->request_type = $request->request_type;
            $tableRequest->remark = $request->remark;

            $tableRequest->save();

            return response()->json([
                'status' => 200,
                'message' => 'Successfully Promoted',
                'data' => $tableRequest
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'The phone you typed is not in our database,'
            ]);
        }
    }

    public function logout()
    {
        $result = auth()->logout();
        return response()->json([
            'status' => 200,
            'message' => 'User successfully Logout!',
            'result' => $result
        ]);
    }

    // Refresh a token, which invalidates the current one
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'status' => 200,
            'message' => 'Login Success',
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL(),
            'user' => auth()->user()
        ]);
    }
}
