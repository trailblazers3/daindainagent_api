<?php

namespace App\Http\Controllers\Api\Noti;

use Pusher\Pusher;
use App\Models\TwodSalelist;
use Illuminate\Http\Request;
use App\Models\ThreedSalelist;
use App\Models\LonepyaingSalelist;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

/**
 * Notifications from referee to agent
 */

class NotificationCenterController extends Controller
{
    public function getAllNotifications()
    {
        if (auth()->user()) {
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                env('PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'),
                $options
            );

            $data = [];

            $last_week = Carbon::now()->subDays(7);

            $twod_sale_lists = TwodSalelist::where('status', 1)
                ->whereBetween('created_at', [$last_week, Carbon::now()])
                ->with('twod')->get();

            $threed_sale_lists = ThreedSalelist::where('status', 1)
                ->whereBetween('created_at', [$last_week, Carbon::now()])
                ->with('threed')->get();

            $lonepyaing_sale_lists = LonepyaingSalelist::where('status', 1)
                ->whereBetween('created_at', [$last_week, Carbon::now()])
                ->with('lonepyaing')->get();

            $data = ['twod_salelists' => $twod_sale_lists, 'threed_salelists' => $threed_sale_lists, 'lonepyaing_salelists' => $lonepyaing_sale_lists];

            $pusher->trigger('notify-channel', 'App\\Events\\Notify', $data);

            return response()->json([
                'status' => 200,
                'accepted_salelists' => $data
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Session Expired! Login Again!'
            ]);
        }
    }

    // Daily Notifications For Noti icon
    public function getCurrentNotifications()
    {
        if (auth()->user()) {
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                env('PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'),
                $options
            );

            $data = [];

            $twod_sale_lists = TwodSalelist::where('status', 1)->latest()->take(50)->get();
            $threed_sale_lists = ThreedSalelist::where('status', 1)->latest()->take(50)->get();
            $lonepyaing_sale_lists = LonepyaingSalelist::where('status',1)->latest()->take(50)->get();


            $data = ['twod_salelists' => $twod_sale_lists, 'threed_salelists' => $threed_sale_lists, 'lonepyaing_salelists' => $lonepyaing_sale_lists];

            $pusher->trigger('notify-channel', 'App\\Events\\Notify', $data);

            return response()->json([
                'status' => 200,
                'accepted_salelists' => $data
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Session Expired! Login Again!'
            ]);
        }
    }
}


