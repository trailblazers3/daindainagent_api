<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Pusher\Pusher;
use App\Models\Twod;
use App\Models\Threed;

use App\Models\LonePyaing;
use App\Models\TwodSalelist;
use Illuminate\Http\Request;
use App\Models\ThreedSalelist;
use App\Models\LonepyaingSalelist;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TwodThreeDController extends Controller
{
    //  2d numbers For AM
    public function getTwoDsAM()
    {
        // current time
       //return  Carbon::now('Asia/Yangon')->toTimeString();
        // $options = array(
        //     'cluster' => env('PUSHER_APP_CLUSTER'),
        //     'encrypted' => true
        // );
        // $pusher = new Pusher(
        //     env('PUSHER_APP_KEY'),
        //     env('PUSHER_APP_SECRET'),
        //     env('PUSHER_APP_ID'),
        //     $options
        // );
        $currenDate = Carbon::now('Asia/Yangon')->toDateString();

        // $current_time = Carbon::now('Asia/Yangon')->toTimeString();
        // $round = $current_time <= '11:10:00' ? 'am' : 'pm';

        $twods = Twod::where('date', $currenDate)->where('round', 'morning')->get();

        // $pusher->trigger('notify-channel', 'App\\Events\\Notify', $twods);
        return response()->json([
            'status' => 200,
            'data' => $twods
        ]);
    }

    //  2d numbers For PM
    public function getTwoDsPM()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $currenDate = Carbon::now()->toDateString();
        $twods = Twod::where('date', $currenDate)->where('round', 'pm')->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $twods);
        return response()->json([
            'status' => 200,
            'data' => $twods
        ]);
    }



    // 3d numbers
    public function getThreeDs()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $currenDate = Carbon::now('Asia/Yangon')->toDateString();
        $threed = Threed::where('date', $currenDate)->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $threed);
        return response()->json([
            'status' => 200,
            'data' => $threed
        ]);
    }

    // Lonepyaing numbers For AM
    public function getLonePyaingsAM()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $currenDate = Carbon::now()->toDateString();
        $lonepyaings = LonePyaing::where('date', $currenDate)->where('round', 'am')->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $lonepyaings);
        return response()->json([
            'status' => 200,
            'data' =>  $lonepyaings
        ]);
    }

    // Lonepyaing numbers For PM
    public function getLonePyaingsPM()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $currenDate = Carbon::now()->toDateString();
        $lonepyaings = LonePyaing::where('date', $currenDate)->where('round', 'pm')->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $lonepyaings);
        return response()->json([
            'status' => 200,
            'data' =>  $lonepyaings
        ]);
    }

    // 2D Sale List
    public function twoDSale(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'twod_id' => 'required',
        //     'client_id' => 'required',
        //     'sale_amount' => 'required',
        //     'customer_name' => 'required',
        //     'customer_phone' => 'required'
        // ]);


        // if($validator->fails()) {
        //     return response()->json([
        //         'status' => 400,
        //         'message' => 'Validation Error!',
        //         'data' => $validator->errors()
        //     ]);
        // }

        $sale_lists = $request->all(); // json string
        $sale_lists =  json_decode(json_encode($sale_lists));  // change to json object from json string

        foreach ($sale_lists->twoDSalesList as $sale) {
            $twod_sale_list = new TwodSalelist();

            $twod_sale_list->twod_id = $sale->twod_id;
            $twod_sale_list->client_id = $sale->client_id;
            $twod_sale_list->sale_amount = $sale->sale_amount;
            $twod_sale_list->customer_name = $sale->customer_name;
            $twod_sale_list->customer_phone = $sale->customer_phone;

            $twod_sale_list->save();
        }

        return response()->json([
            'status' => 200,
            'message' => "TwoD Lists added successfully!"
        ]);
    }

    // 3D Sale List
    public function threeDSale(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'threed_id' => 'required',
        //     'client_id' => 'required',
        //     'sale_amount' => 'required',
        //     'customer_name' => 'required',
        //     'customer_phone' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json([
        //         'status' => 400,
        //         'message' => 'Validation Error!',
        //         'data' => $validator->errors()
        //     ]);
        // }

        $sale_lists = $request->all(); // json string
        $sale_lists =  json_decode(json_encode($sale_lists));  // change to json object from json string

        foreach ($sale_lists->threeDSalesList as $sale) {
            $threed_sale_list = new ThreedSalelist();

            $threed_sale_list->threed_id = $sale->threed_id;
            $threed_sale_list->client_id = $sale->client_id;
            $threed_sale_list->sale_amount = $sale->sale_amount;
            $threed_sale_list->customer_name = $sale->customer_name;
            $threed_sale_list->customer_phone = $sale->customer_phone;

            $threed_sale_list->save();
        }

        return response()->json([
            'status' => 200,
            'message' => "ThreeD Lists added successfully!"
        ]);
    }

    public function lonePyaingSale(Request $request)
    {
        $sale_lists = $request->all(); // json string
        $sale_lists =  json_decode(json_encode($sale_lists));  // change to json object from json string

        foreach ($sale_lists->lonePyaingSalesList as $sale) {
            $lonepyaing_sale_list = new lonepyaingSalelist();

            $lonepyaing_sale_list->lonepyaing_id = $sale->lonepyaing_id;
            $lonepyaing_sale_list->client_id = $sale->client_id;
            $lonepyaing_sale_list->sale_amount = $sale->sale_amount;
            $lonepyaing_sale_list->customer_name = $sale->customer_name;
            $lonepyaing_sale_list->customer_phone = $sale->customer_phone;

            $lonepyaing_sale_list->save();
        }

        return response()->json([
            'status' => 200,
            'message' => "LonePyaing Lists added successfully!"
        ]);
    }

    public function ShowTwoDSaleLists()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $sale_lists = TwodSalelist::where('status', 0)->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $sale_lists);
        return response()->json([
            'status' => 200,
            'data' => $sale_lists
        ]);
    }

    public function ShowThreeDSaleLists()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $sale_lists = ThreedSalelist::where('status', 0)->get();


        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $sale_lists);
        return response()->json([
            'status' => 200,
            'data' => $sale_lists
        ]);
    }

    public function ShowLonePyaingSaleLists()
    {

        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $sale_lists = LonepyaingSalelist::where('status', 0)->get();

        $pusher->trigger('notify-channel', 'App\\Events\\Notify', $sale_lists);
        return response()->json([
            'status' => 200,
            'data' => $sale_lists
        ]);
    }
}
