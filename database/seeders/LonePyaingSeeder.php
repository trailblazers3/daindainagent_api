<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\LonePyaing;
use Illuminate\Database\Seeder;


define('CURRENT_DATE', Carbon::now('Asia/Yangon')->toDateString());


class LonePyaingSeeder extends Seeder
{
    protected $lonepyaings = [
        [
            'number' => "*0",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*1",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*2",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*3",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*4",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 2
        ],
        [
            'number' => "*5",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*6",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*7",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*8",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "*9",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "1*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "2*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "3*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "4*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "5*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "6*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "7*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "8*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => "9*",
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->lonepyaings as $lonepyaing) {
            LonePyaing::create($lonepyaing);
        }
    }
}
