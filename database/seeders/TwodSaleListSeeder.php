<?php

namespace Database\Seeders;

use App\Models\TwodSalelist;
use Illuminate\Database\Seeder;

class TwodSaleListSeeder extends Seeder
{
    protected $twod_salelists = [
        [
            'twod_id' => 1,
            'client_id' => 1,
            'sale_amount' => 3000,
            'customer_name' => 'Spiderman',
            'customer_phone' => '09555666777',
            'status' => 0
        ],
        [
            'twod_id' => 2,
            'client_id' => 1,
            'sale_amount' => 5500,
            'customer_name' => 'Superman',
            'customer_phone' => '09123666777',
            'status' => 0
        ],
        [
            'twod_id' => 3,
            'client_id' => 1,
            'sale_amount' => 1500,
            'customer_name' => 'Spiderman',
            'customer_phone' => '09555666777',
            'status' => 0
        ],
        [
            'twod_id' => 4,
            'client_id' => 1,
            'sale_amount' => 2500,
            'customer_name' => 'Captain America',
            'customer_phone' => '09300300300',
            'status' => 1
        ],
        [
            'twod_id' => 5,
            'client_id' => 1,
            'sale_amount' => 1000,
            'customer_name' => 'Iron man',
            'customer_phone' => '09400400400',
            'status' => 1
        ],
        [
            'twod_id' => 6,
            'client_id' => 1,
            'sale_amount' => 3000,
            'customer_name' => 'Captain America',
            'customer_phone' => '09555666777',
            'status' => 2
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->twod_salelists as $twod_salelist) {
            TwodSalelist::create($twod_salelist);
        }
    }
}

