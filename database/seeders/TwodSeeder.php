<?php

namespace Database\Seeders;

use App\Models\Twod;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


define('CURRENT_DATE', Carbon::now('Asia/Yangon')->toDateString());


class TwodSeeder extends Seeder
{
     protected $twods = [
        [
            'number' => 00,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 01,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 25,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 37,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 2
        ],
        [
            'number' => 56,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 76,
            'max_amount' => 150000,
            'compensation' => 80,
            'date' => CURRENT_DATE,
            'round' => 'morning',
            'status' => 1,
            'client_id' => 1
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->twods as $twod) {
            Twod::create($twod);
        }
    }
}
