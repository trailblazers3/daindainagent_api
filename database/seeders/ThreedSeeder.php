<?php

namespace Database\Seeders;

use App\Models\Threed;
use Carbon\Carbon;
use Illuminate\Database\Seeder;


define('CURRENT_DATE', Carbon::now('Asia/Yangon')->toDateString());

class ThreedSeeder extends Seeder
{
    protected $threeds = [
        [
            'number' => 102,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' =>  CURRENT_DATE,
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 845,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' => CURRENT_DATE,
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 925,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' => CURRENT_DATE,
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 737,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' => CURRENT_DATE,
            'status' => 1,
            'client_id' => 2
        ],
        [
            'number' => 956,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' => CURRENT_DATE,
            'status' => 1,
            'client_id' => 1
        ],
        [
            'number' => 782,
            'max_amount' => 150000,
            'compensation' => 800,
            'date' => CURRENT_DATE,
            'status' => 1,
            'client_id' => 1
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->threeds as $threed) {
            Threed::create($threed);
        }
    }
}
