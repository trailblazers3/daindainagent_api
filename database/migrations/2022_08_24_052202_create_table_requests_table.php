<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_requests', function (Blueprint $table) {
            $table->id();
            $table->string('client_id')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('referee_id')->nullable();
            $table->string('operationstaff_id')->nullable();
            $table->string('request_type');
            $table->integer('status')->default(0); // 0 => pending, 1 => accept, 2 => reject
            $table->longText('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_requests');
    }
}
