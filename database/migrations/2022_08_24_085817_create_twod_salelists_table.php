<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwodSalelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twod_salelists', function (Blueprint $table) {
            $table->id();
            $table->integer('twod_id');
            $table->integer('client_id');
            $table->bigInteger('sale_amount');
            $table->boolean('winning_status')->default(0); //0 => loose, 1 => win
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->tinyInteger('status')->default(0); // 0 => pending, 1 => success, 2 => error
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twod_salelists');
    }
}
