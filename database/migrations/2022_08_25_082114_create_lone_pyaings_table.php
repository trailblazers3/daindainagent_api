<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLonePyaingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lone_pyaings', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->bigInteger('max_amount');
            $table->bigInteger('compensation'); // za
            $table->date('date');
            $table->string('round');
            $table->boolean('status');
            $table->integer('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lone_pyaings');
    }
}
