<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twods', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->bigInteger('max_amount');
            $table->bigInteger('compensation'); // za;
            $table->date('date');
            $table->string('round');
            $table->boolean('status'); // 0 and 1 , 0 => ma ya | 1 => ya
            $table->integer('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twods');
    }
}
