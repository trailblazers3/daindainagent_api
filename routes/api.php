<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TwodThreeDController;
use App\Http\Controllers\Api\Pusher\TwoDController;
use App\Http\Controllers\Api\Pusher\ThreeDController;
use App\Http\Controllers\Api\Pusher\LonePyaingController;
use App\Http\Controllers\Api\AcceptedTransitionController;
use App\Http\Controllers\Api\PusherNotificationController;
use App\Http\Controllers\Api\TwoD\TwodManagementController;
use App\Http\Controllers\Api\Pusher\TwoDSaleListsController;
use App\Http\Controllers\Api\Noti\NotificationCenterController;
use App\Http\Controllers\Api\SaleDayBook\SaleDayBookController;
use App\Http\Controllers\Api\WinningResult\WinningResultController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::post('checkPhone', [AuthController::class, 'checkPhone']);


Route::get('/winning-result', [WinningResultController::class, 'checkResult']);

Route::group(['middleware' => 'api'], function () {

    Route::get('agent-profile', [AuthController::class, 'agentProfile']);
    Route::post('profile-update', [AuthController::class, 'profileUpdate']);
    Route::post('request-promotion', [AuthController::class, 'promoteRequest']);

    // Get 2Ds, 3Ds, longpyaings (from referee to agents)
    Route::get('/getTwoDsAM', [TwodThreeDController::class, 'getTwoDsAm']);
    Route::get('/getTwoDsPM', [TwodThreeDController::class, 'getTwoDsPM']);
    Route::get('/3ds', [TwodThreeDController::class, 'getThreeDs']);
    Route::get('/getLonePyaingsAM', [TwodThreeDController::class, 'getLonePyaingsAM']);
    Route::get('/getLonePyaingsPM', [TwodThreeDController::class, 'getLonePyaingsPM']);

    // Store 2d sale lists ,3d sale lists, lonepyaing sale lists
    Route::post('2d-sale', [TwodThreeDController::class, 'twoDSale']);
    Route::post('3d-sale', [TwodThreeDController::class, 'threeDSale']);
    Route::post('lonepyaing-sale', [TwodThreeDController::class, 'lonePyaingSale']);

    // show pending bet lists from
    Route::get('lonepyaing-salelists', [TwodThreeDController::class, 'ShowLonePyaingSaleLists']);
    Route::get('2d-salelists', [TwodThreeDController::class, 'ShowTwoDSaleLists']);
    Route::get('3d-salelists', [TwodThreeDController::class, 'ShowThreeDSaleLists']);

    // show 2d sale lists
    Route::get('/2d-noti', [TwoDController::class, 'notification']);
    Route::get('/3d-noti', [ThreeDController::class, 'notification']);
    Route::get('/lonepyaing-noti', [LonePyaingController::class, 'notification']);

    // Route::get('/3d-salelists-noti', [TwoDSaleListsController::class, 'twod']);
    // Accepted Transitions
    Route::get('/2d-accepted-transition', [AcceptedTransitionController::class, 'twodAcceptedTransitions']);
    Route::get('/3d-accepted-transition', [AcceptedTransitionController::class, 'threedAcceptedTransitions']);
    Route::get('/lonepyaing-accepted-transition', [AcceptedTransitionController::class, 'lonepyaingAcceptedTransitions']);

    //Winning Result


    // Notifications
    Route::get('/all-notifications', [NotificationCenterController::class, 'getAllNotifications']); // For Notification center
    Route::get('/current-notifications', [NotificationCenterController::class, 'getCurrentNotifications']);

    Route::get('/twod-salesday-book', [SaleDayBookController::class, 'twoDSaleDayBook']);

    Route::post('logout', [AuthController::class, 'logout']);
});
